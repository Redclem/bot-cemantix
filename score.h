#ifndef SCORE_H_INCLUDED
#define SCORE_H_INCLUDED

#include <cstdlib>

#include <string>
#include <sstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Easy.hpp>

constexpr float score_not_found = -std::numeric_limits<float>::infinity();
constexpr const char detector[] = "\"score\":";
constexpr size_t detector_size = sizeof(detector) / sizeof(detector[0]) - 1;

constexpr const char * cemantix_url = "https://cemantix.herokuapp.com/score";

float score_mot(std::string word);

#endif // SCORE_H_INCLUDED
