#include <iostream>
#include <string>

#include <queue>
#include <set>

#include <chrono>

#include "score.h"
#include "model.h"
#include "word_manager.h"


using namespace std;

void init()
{
    init_model();
}

int main()
{
    init();

    std::cout << "Mot de départ : ";

    std::string mot_depart;

    std::cin >> mot_depart;

    WordManager mots;
    std::vector<std::pair<std::string, float>> proches;

    std::chrono::system_clock::time_point time_beg = std::chrono::system_clock::now();

    mots.emplace(mot_depart);

    while (!mots.empty() && mots.top().get_score() != 1.0f)
    {
        mots.find_around_best();
    }

    std::chrono::system_clock::time_point time_end = std::chrono::system_clock::now();

    std::cout << std::endl << std::endl << "---------------------------------------\n";

    std::cout << "Temps : " << static_cast<float>(std::chrono::duration_cast<std::chrono::microseconds>(time_end - time_beg).count()) / 1.0e6f << "s\n";

    if(mots.empty())
    {
        std::cout << "Pas trouvé\n";
    }
    else
    {
        std::cout << "Trouvé : " << mots.top().get_word() << '\n';
        std::cout << mots.words_seen() << " mots essayés\n";
    }


    return 0;
}
