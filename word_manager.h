#ifndef WORD_MANAGER_H_INCLUDED
#define WORD_MANAGER_H_INCLUDED

#include "model.h"

#include <cstdlib>

#include <vector>
#include <set>
#include <utility>
#include <string>
#include <sstream>
#include <thread>
#include <chrono>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Option.hpp>
#include <curlpp/Multi.hpp>

#include <word2vec.hpp>

class WordManager
{
public:

    float emplace(const std::string & word);

    bool empty() const {return m_words.empty();}

    Word & top() {return m_words.front();}
    const Word & top() const {return m_words.front();}

    size_t words_seen() const {return m_words_seen.size();}

    size_t queue_size() const {return m_words.size();}

    void pop()
    {
        std::pop_heap(m_words.begin(), m_words.end());

        m_words.pop_back();
    }

    void control_size() {if(m_words.size() > 100) m_words.resize(50);}

    void push_queue(const Word & word)
    {
        m_words.push_back(std::move(word));

        std::push_heap(m_words.begin(), m_words.end());

        control_size();
    }

    void push_queue(Word && word)
    {
        m_words.push_back(std::move(word));

        std::push_heap(m_words.begin(), m_words.end());

        control_size();
    }


    void find_around_best();

private:
    std::vector<Word> m_words;
    std::set<std::string> m_words_seen;

    std::vector<std::pair<std::string, float>> m_proches;
};


#endif // WORD_MANAGER_H_INCLUDED
