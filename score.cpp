#include "score.h"


float score_mot(std::string word)
{
    curlpp::Easy request;

    request.setOpt(new curlpp::options::Url(cemantix_url));
    request.setOpt(new curlpp::options::PostFields("word=" + word));

    std::ostringstream outstream;
    outstream << request;

    std::string out = outstream.str();
    size_t idx = out.find(detector);

    return idx == std::string::npos ? score_not_found : strtof(out.c_str() + detector_size + idx, nullptr);
}

