#ifndef MODEL_H_INCLUDED
#define MODEL_H_INCLUDED

#include <memory>
#include <iostream>
#include <limits>
#include <utility>

#include <word2vec.hpp>

#include "score.h"

constexpr bool log_requests = false;

class Word
{
public:
    static std::unique_ptr<w2v::w2vModel_t> modele;

    Word() : m_word(), m_search_size(0), m_search_begin(0), m_score(score_not_found) {}

    Word(const std::string & word) : m_word(word), m_search_size(10), m_search_begin(0), m_score(score_mot(word)) {
        if constexpr (log_requests)
        {
            if(m_score != score_not_found)
                std::cout << word << ':' << m_score << std::endl;
        }

    }

    Word(std::string && word, float score) : m_word(std::move(word)), m_search_size(10), m_search_begin(0), m_score(score) {}

    const std::string & get_word() const {return m_word;}
    float get_score() const {return m_score;}
    size_t get_search_size() const {return m_search_size;}
    size_t get_search_begin() const {return m_search_begin;}

    bool found() const {return m_score != score_not_found;}

    w2v::word2vec_t to_vec() const {return {modele, m_word};}

    bool operator<(const Word & rhs) const {return m_score < rhs.m_score;}

    void update_search_size() {m_search_begin += m_search_size; m_search_size *= 2;}


private:
    std::string m_word;
    size_t m_search_size;
    size_t m_search_begin;
    float m_score;
};

void init_model();

#endif // MODEL_H_INCLUDED
