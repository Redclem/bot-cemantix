#include "word_manager.h"

float WordManager::emplace(const std::string & word)
{
    if(!m_words_seen.contains(word))
    {
        m_words_seen.insert(word);

        Word mot(word);

        if(mot.found())
        {
            m_words.push_back(std::move(mot));

            std::push_heap(m_words.begin(), m_words.end());

            return mot.get_score();

            control_size();
        }
        return score_not_found;
    }
    return score_not_found;
}

void WordManager::find_around_best()
{
    Word keyword = top();

    pop();

    auto init_size = m_words.size();

    std::cout << "Recherche avec: " << keyword.get_word() << ", intervale: [" << keyword.get_search_begin() << "," << (keyword.get_search_begin() + keyword.get_search_size()) << "), score: " << keyword.get_score() <<  std::endl;


    Word::modele->nearest(keyword.to_vec(), m_proches, keyword.get_search_size() + keyword.get_search_begin());

    std::vector<curlpp::Easy> requests(keyword.get_search_size());
    std::vector<std::ostringstream> outputs(keyword.get_search_size());

    curlpp::options::Url url_op(cemantix_url);

    curlpp::Multi multi_req;

    for(size_t i = 0; i != keyword.get_search_size(); ++i)
    {
        requests[i].setOpt(url_op);
        requests[i].setOpt(new curlpp::options::PostFields("word=" + m_proches[i + keyword.get_search_begin()].first));
        requests[i].setOpt(new curlpp::options::WriteStream(&outputs[i]));

        multi_req.add(&(requests[i]));
    }

    int rem;

    do
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
        if(!multi_req.perform(&rem))
        {
            throw std::runtime_error("Erreur multi requete");
        }
    } while (rem);

    for(size_t i = 0; i != keyword.get_search_size(); ++i)
    {
        size_t idx_proches = i + keyword.get_search_begin();

        std::string out = outputs[i].str();

        size_t idx = out.find(detector);

        if(m_words_seen.insert(m_proches[idx_proches].first).second && idx != std::string::npos)
        {
            Word mot(std::move(m_proches[idx_proches].first), strtof(out.c_str() + detector_size + idx, nullptr));

            if constexpr (log_requests)
                std::cout << mot.get_word() << ':' << mot.get_score() << '\n';

            m_words.push_back(std::move(mot));

        }
    }

    keyword.update_search_size();

    m_words.push_back(keyword);

    for(size_t i = init_size + 1; i <= m_words.size(); ++i)
    {
        std::push_heap(m_words.begin(), m_words.begin() + i);
    }

    control_size();
}
