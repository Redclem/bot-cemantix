#include "model.h"

std::unique_ptr<w2v::w2vModel_t> Word::modele = std::unique_ptr<w2v::w2vModel_t>();

void init_model()
{
    Word::modele.reset(new w2v::w2vModel_t);

    if(!Word::modele->load("frWac_no_postag_no_phrase_700_skip_cut50.bin"))
    {
        throw std::runtime_error(Word::modele->errMsg());
    }
}
